# LHCb Analysis Production submission

Welcome to the **Analysis Productions** data packages repository.
This repository is used by analysts and working groups (WGs) to upload options files, configure, validate and submit Analysis Productions.

If you are looking for the magic behind the scenes, please see the [`LbAnalysisProductions` repository](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/LbAnalysisProductions) and related repositories under the [WP2 Analysis Productions "hat"](https://gitlab.cern.ch/lhcb-dpa/analysis-productions).

## Documentation

If you need help and/or guidance, documentation is available on the site [https://lhcb-ap.docs.cern.ch/](https://lhcb-ap.docs.cern.ch/).

### [How do I create an Analysis Production?](https://lhcb-ap.docs.cern.ch/user_guide/creating.html#creating-an-analysis-production)

### [How can I find the code used in previous productions?](https://lhcb-ap.docs.cern.ch/user_guide/creating.html#based-on-a-previous-version)

### [How do I submit?](https://lhcb-ap.docs.cern.ch/user_guide/submitting.html)

### [Where do I find the available productions?](https://lhcb-analysis-productions.web.cern.ch/productions/)
